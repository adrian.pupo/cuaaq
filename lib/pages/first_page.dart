import 'package:flutter/material.dart';
import 'package:flutter_application_1/pages/screens.dart';


class FirstScreen extends StatefulWidget {
   
  const FirstScreen({Key? key}) : super(key: key);

  @override
  State<FirstScreen> createState() => _FirstScreenState();
}

class _FirstScreenState extends State<FirstScreen> {

  @override
  void initState(){
    super.initState();
    _navigatetohome();
  }

  _navigatetohome() async{
    await Future.delayed(const Duration(seconds: 1), (){});
    Navigator.pushReplacement(
      context, MaterialPageRoute(builder: (context) => const TermsScreen()));
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
           child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
             children: const[
               SizedBox(height: 230),
               Image(image: AssetImage('Grupo_2579.png')),
               SizedBox(height: 250),
               Text('From', style: TextStyle(fontSize: 16)),
               SizedBox(height: 10,),
               Image(image: AssetImage('Grupo_1370.png')),
             ],
           ),
        ),
    );
  }
}