import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:async';

class MapaScreen extends StatefulWidget {
   
  @override
  State<MapaScreen> createState() => _MapaScreenState();
  static const String routeName = 'mapa';
}

class _MapaScreenState extends State<MapaScreen> {
  final Completer<GoogleMapController> _controller = Completer();
  MapType mapType = MapType.normal;

  @override
  Widget build(BuildContext context) {
    const CameraPosition puntoinicial = CameraPosition(
      target: LatLng(-23.661607, -70.399907), zoom: 17,
      //tilt: 50
    );

    Set<Marker> markers = <Marker>{};
    markers.add(const Marker(
        markerId: MarkerId('geo-location'),
        position: LatLng(-23.661607, -70.399907)));

    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xff7B279E),
        actions: [
          IconButton(
              icon: const Icon(Icons.location_disabled),
              onPressed: () async {
                final GoogleMapController controller = await _controller.future;
                controller.animateCamera(
                    CameraUpdate.newCameraPosition(const CameraPosition(
                  target: LatLng(-23.661607, -70.399907),
                  zoom: 17,
                  //tilt: 50
                )
                )
                );
              }),
        ],
      ),
      drawer: _crearMenu(),
      body: GoogleMap(
        myLocationButtonEnabled: true,
        mapType: mapType,
        markers: markers,
        initialCameraPosition: puntoinicial,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
      ),

      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.white,
        foregroundColor: const Color(0xff7B279E),
        child: const Icon(Icons.layers),
        onPressed: () {
          if (mapType == MapType.normal) {
            mapType = MapType.satellite;
          } else {
            mapType = MapType.normal;
          }

          setState(() {});
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
    );
  }



  Drawer _crearMenu() {
    return Drawer(
      child: ListView(
        children: <Widget> [
          const DrawerHeader(
              decoration: BoxDecoration(
                color: Color(0xff7B279E),
              ),
              child: Text('Menu',
                  style: TextStyle(color: Colors.black, fontSize: 20))),
          ListTile(
            leading: const Icon(Icons.add_circle_outline, color: Color(0xff7B279E)),
            title: const Text('Contribuir',
                style: TextStyle(color: Color(0xff676e71), fontSize: 20)),
            onTap: () {
              Navigator.pushNamed(context, 'mapa');
            },
          )
        ],
      ),
    );
  }

}