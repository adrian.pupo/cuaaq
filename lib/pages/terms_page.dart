import 'package:flutter/material.dart';
import 'package:flutter_application_1/pages/screens.dart';

class TermsScreen extends StatelessWidget {
   
  const TermsScreen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: const _text(),
        backgroundColor: Colors.white,
        toolbarHeight: 170,
        titleSpacing: 16,
      ),
      body: const _listview(),
      backgroundColor: Colors.white,
      bottomNavigationBar: const _bottomappbar(),
    );
  }
}

class _bottomappbar extends StatelessWidget {
  const _bottomappbar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      shape: const CircularNotchedRectangle(),
      child: Container(
        margin: const EdgeInsets.symmetric(
          vertical: 30, horizontal: 30
        ),
        child: TextButton(
          style: TextButton.styleFrom(
                backgroundColor: Color(0xff004efc), shape: StadiumBorder()),
          onPressed: (){
            Navigator.pushNamed(context, 'mapa');
          },
          child: const Padding(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Text('Acepto los términos y condiciones',
            style: TextStyle(color: Colors.white, fontSize: 18),),
          ),
        ),
      ),
    );
  }
}

class _listview extends StatelessWidget {
  const _listview({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 0, horizontal: 25),
      child: ListView(
        children: const [
          ListTile(
            title: Text(
              'Los presentes Términos y Condiciones rigen la utilización del software o aplicación “app” Cuaaq y la plataforma web de Cuaaq. La marca y el logo de Cuaaq son una marca creada a nombre de IDAC EIRL. Nuestro domicilio legal se encuentra en Antonio Poupin 839, Antofagasta, Chile. Al seleccionar la acción de confirmación declaras haber leído y aceptado los presentes Términos y Condiciones así como el tratamiento de datos personales que realiza CityHome y que se encuentra contenido en nuestra Política de Privacidad. El software CityHome es propiedad intelectual propietaria y exclusiva de IDAC EIRL. La modificación, traducción, adaptación, edición, o cualquier acto de ingeniería inversa, desensamblaje o descompilación de su código se encuentra prohibido sin la autorización expresa de IDAC EIRL. Condiciones de uso: CityHome es un servicio gratuito para individuos que se otorga por medio del software de CityHome y su red social. Nos reservamos el derecho a denegar o restringir el acceso a la red social y/o al software de IDAC o modificar las prestaciones que entregamos en cualquier momento y por cualquier razón. La violación a estos Términos y Condiciones o a nuestra Política de Privacidad será causal suficiente para poner término permanente a la utilización de nuestros servicios. El uso no autorizado de la propiedad intelectual o de la marca de CityHome constituirá también una infracción suficiente para poner término a la utilización de nuestros servicios. Al aceptar el presente acuerdo reconoces que la plataforma de CityHome constituye una red social donde su contenido es aportado tanto por los usuarios como por las entidades públicas o privadas que la componen. Las estimaciones de tiempo de espera en una fila y el contenido de los comentarios en la plataforma son exclusiva responsabilidad de quienes los emiten. Cualquier modificación de estos Términos y Condiciones, o de la Política de Privacidad que rige el tratamiento de datos de CityHome, regirá desde su publicación en nuestro sitio web. Cuando dichas modificaciones impacten de manera significativa en el tratamiento de los datos personales que realiza CityHome o el funcionamiento de la aplicación esta será previamente notificada por medio de la interfaz de la aplicación.',
              textAlign: TextAlign.justify
            ),
          )
        ],
      ));
  }
}


class _text extends StatelessWidget {
  const _text({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 25, vertical: 46),
      child: const Text('Antes de ingresar a Cuaaq debemos informarte sobre nuestros términos y condiciones.', 
      textAlign: TextAlign.justify,
      maxLines: 100,
      style: TextStyle(color: Colors.black, fontSize: 20, fontStyle:FontStyle.normal),
      ),
    );
  }
}