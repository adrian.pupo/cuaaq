import 'package:flutter/material.dart';
import 'package:flutter_application_1/screens/background.dart';
import 'package:flutter_application_1/screens/screens.dart';

class HomeScreen extends StatelessWidget {
   
  const HomeScreen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
           children: const [
            Background(),
            CardTable(),            
           ],

         ),
  
    );
  }
}