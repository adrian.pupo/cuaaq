import 'package:flutter/material.dart';

class Background extends StatelessWidget {
   
  const Background({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(decoration: _boxdecoration()),        
        const _image(),
        const _Sofa(),
        const _text(),
        
      ],
    );
  }

  BoxDecoration _boxdecoration() {
    return const BoxDecoration(
      gradient: LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        stops: [0.05, 0.15],
        colors: [
          Color.fromARGB(255, 248, 244, 246),
          Color.fromARGB(255, 246, 246, 248),
         ],
      )
    );
  }
}

class _image extends StatelessWidget {
  const _image({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      alignment:  Alignment.topRight,
      child: Image(image: AssetImage('Grupo_2314_3.png')));
  }
}

class _Sofa extends StatelessWidget {
  const _Sofa({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 8),
      alignment:  Alignment.topRight,    
      child: const Image(image: AssetImage('Enmascarar_1.png')),);
  }
}

class _text extends StatelessWidget {
  const _text({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 26, horizontal: 26),
      alignment:  Alignment.topLeft,                
      child: const Text('¿Qué lugar visitaras hoy?', style: TextStyle(color: Colors.white,fontSize: 24, fontWeight: FontWeight.bold)));
  }
}