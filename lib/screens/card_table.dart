import 'package:flutter_application_1/contentful.dart';
import 'package:flutter_application_1/module/box.dart';
import 'package:flutter_application_1/sercive/service_contentful.dart';
import 'package:contentful/client.dart';
import 'package:flutter/material.dart';


class CardTable extends StatefulWidget {
  const CardTable({ Key? key }) : super(key: key);

  @override
  State<CardTable> createState() => _CardTableState();
}

class _CardTableState extends State<CardTable> {

  final repo = EventRepository(Client(BearerTokenHTTPClient('B9SUv1dgYGgItrtS4ix5jX1Rd5kioSVGG5bd1eB0QJ0'),
    spaceId: 'oxzain8j0qg1',
  ));

  @override
  Widget build(BuildContext context) {

    final size = MediaQuery.of(context).size;
    return Column(
      children: [
        _title(),
         Container(
          padding: const EdgeInsets.symmetric(vertical: 10),
          height: size.height*0.5,
          width: double.infinity,
          color: Color.fromARGB(255, 248, 242, 242),
          child: Center(
            
          child: FutureBuilder<List<Event>>(
            future: repo.findAllCategory(),
            builder: (BuildContext context, AsyncSnapshot<List<Event>> snapshot){
              //print(snapshot);
              if (snapshot.hasData){
                List<Widget> list = [];
                //print();
                snapshot.data?.forEach((element) { list.add(Box('https:' + element.fields!.icon.fields!.file!.url, element.fields!.title));
                });
                Object? title;
                print(title);
                print(list);


                return GridView.count(
                primary: false,
                crossAxisCount: 1,
                );
                
              }
              else{
                return const Center(
                  child: Text('Cargando...'),
                );
              }
              
            }
            ), 
        ))
      ]
    );
  }
}

class _title extends StatelessWidget {
  const _title({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 117, horizontal: 37),
      alignment:  Alignment.topLeft,
      child: const Text('Categorías',style: TextStyle(color: Colors.black,fontSize: 22, fontWeight: FontWeight.bold)),    
    );
  }
}