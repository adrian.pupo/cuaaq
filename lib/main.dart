import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_application_1/pages/first_page.dart';
import 'package:flutter_application_1/pages/mapa_page.dart';
import 'package:flutter_application_1/pages/screens.dart';
import 'package:flutter_application_1/pages/terms_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false, //Elimin Banner red On
      initialRoute: 'first',
      routes: {
        'first': (_)=> const FirstScreen(),
        'terms': (_)=> const TermsScreen(),    
        'home' : (_)=> const HomeScreen(),  
        'mapa' : (_)=> MapaScreen(), 
      },
      theme: ThemeData(
                primarySwatch: Colors.blue,
      ),
    );
  }
}




